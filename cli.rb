require 'json'
require 'json_schemer'

schema_file = 'bom-1.5.schema.json'
schema_data = JSON.parse(File.read(schema_file))
schemer = JSONSchemer.schema(schema_data)

json_file = 'sbom.json'
json_data = JSON.parse(File.read(json_file))

schemer.validate(json_data).map{|error| puts JSONSchemer::Errors.pretty(error)}